
/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')
module.exports = {
  darkMode: 'class',
  content: [
    './dev/pages/**/*.{html,js}',
    './dev/index.html',
  ],
  theme: {

    extend: {
      fontFamily: {
        'sans': ['Inter var', 'santepheap', ...defaultTheme.fontFamily.sans],
        'fira': ['Fira Code VF', ...defaultTheme.fontFamily.sans],
        'nunito': ['nunito', ...defaultTheme.fontFamily.sans],
        'santepheap': ['santepheap', ...defaultTheme.fontFamily.sans],
        'intervar': ['Inter var', ...defaultTheme.fontFamily.sans],
      },
      backgroundImage: {
        'logo': "url('/assets/img/logo.svg')",
        'logo-white': "url('/assets/img/logo-white.svg')",
      },
      gridTemplateColumns: {
        // Simple 16 column grid
        '16': 'repeat(16, minmax(0, 1fr))',
      },
      colors: {
        primary: {
          light: '#24B1F0',
          DEFAULT: '#0A91D1',
          dark: '#06629a',
          50: '#f0f9ff',
          100: '#DCF0F9',
          200: '#bae6fd',
          300: '#7dd3fc',
          400: '#38bdf8',
          500: '#0ea5e9',
          600: '#067AB6',
          700: '#0369a1',
          800: '#035986',
          900: '#023b5d',
          925: '#02233a',
          950: '#001A27',
          975: '#00080D',
        },
        secondary: {
          light: '#ff57d8',
          DEFAULT: 'rgb(var(--color-secondary) / <alpha-value>)',
          dark: '#A4057F',
          50: '#FEF5FC',
          100: '#FFE9F9',
          200: '#FCC9F1',
          300: '#FA9DE5',
          400: '#F775DA',
          500: '#F43DCA',
          600: '#d21ea9',
          700: '#b6088d',
          800: '#90026F',
          900: '#7D0060',
          925: '#690050',
          950: '#3A002D',
          975: '#200019',
        },
        slate: {
          light: '#7B8AA0',
          DEFAULT: '#141D30',
          dark: '#0C0F1C',
          50: '#f8fafc',
          100: '#f1f5f9',
          200: '#e2e8f0',
          300: '#cbd5e1',
          400: '#94a3b8',
          500: '#64748b',
          600: '#475569',
          700: '#334155',
          800: '#1e293b',
          900: '#0f172a',
          925: '#090F21',
          950: '#020617',
          975: '#00020B',
        },
        'accent': {
          light: '#818cf8',
          DEFAULT: '#5f5af6',
          dark: '#322eaf',
        },
        'navy': {
          100: '#cfd7e7',
          200: '#a3adc2',
          300: '#697a9b',
          400: '#5c6b8a',
          450: '#465675',
          500: '#384766',
          600: '#313e59',
          700: '#26334d',
          800: '#202b40',
          900: '#192132',
        },
      },
      transitionProperty: {
        'size': 'width, height, opacity',
      },
      fontSize: {
        'small': '80%'
      }
    },
    container: {
      center: true,
      // padding: {
      //   DEFAULT: '1rem',
      //   sm: '2rem',
      //   lg: '4rem',
      //   xl: '5rem',
      //   '2xl': '6rem',
      // },
    },
  },
  plugins: [],
  // corePlugins: {
  //   preflight: false,
  // },
}

