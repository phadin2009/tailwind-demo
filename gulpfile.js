const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const sass = require('gulp-sass')(require('sass'));
const rename = require('gulp-rename');
const fileinclude = require('gulp-file-include');
const postcss = require("gulp-postcss"); //For Compiling tailwind utilities with tailwind config
const browserSync = require("browser-sync").create();

function livePreview(done) {
    browserSync.init({
        server: {
            baseDir: './dist',
        },
        port: 5000,
    });
    done();
}

function taskJS(cb) {
    gulp.src('./dev/assets/js/*.js') // all sources js file to task it
        .pipe(concat('dist.main.js')) // concatenate all js files inside one file main.js
        // .pipe(uglify()) // uglify will minify the main.js to make it compressed
        .pipe(gulp.dest('./dist/assets/js/')) // create new main.js file inside dist folder
    console.log("JS Watching for Changes..\n");
    cb();
}

function taskScript(cb) {
    gulp.src('./dev/assets/js/script/*.js')
        .pipe(gulp.dest('./dist/assets/js/'))
    console.log("Script Watching for Changes..\n");
    cb();
}

function taskCSS(cb) {
    gulp.src('./dev/assets/scss/main.scss')
        .pipe(sass().on("error", sass.logError))
        .pipe(postcss([
            require('tailwindcss'), // No need declaration above
            require('autoprefixer'), // No need declaration above
        ]))
        .pipe(rename('dist.main.css')) // rename file
        .pipe(gulp.dest('./dist/assets/css/'));
    console.log("CSS Watching for Changes..\n");
    cb();
}

function taskHTML(cb) {
    // gulp.src(['./dev/**/*.html', '!./dev/pages/inc/**/*.html'])
    gulp.src('./dev/**/*.html')
        .pipe(fileinclude())
        .pipe(gulp.dest('./dist/'));
    console.log("HTML Watching for Changes..\n");
    cb();
}

function devImages(cb) {
    gulp.src('./dev/assets/img/**/*')
        .pipe(gulp.dest('./dist/assets/img/'));
    cb();
}

function devFonts(cb) {
    gulp.src('./dev/assets/font/**/*')
        .pipe(gulp.dest('./dist/assets/font/'));
    cb();
}

function watchFiles() {
    gulp.watch(
        `./dev/**/*.html`,
        gulp.series(taskHTML, taskCSS)
    );
    gulp.watch(
        `./dev/assets/scss/**/*.scss`,
        gulp.series(taskCSS)
    );
    gulp.watch(
        `./dev/assets/js/**/*.js`,
        gulp.series(taskJS)
    );
    gulp.watch(
        `./dev/assets/js/script/*.js`,
        gulp.series(taskScript)
    );
    gulp.watch(
        `./dev/assets/img/**/*`,
        gulp.series(devImages)
    );
    gulp.watch(
        `./dev/assets/font/**/*`,
        gulp.series(devFonts)
    );
}

// exports.css = gulp.series(css);
// exports.html = gulp.series(html);
//
// exports.build = gulp.parallel(css, html);
//   exports.default = gulp.parallel(css, html);
exports.default = gulp.series(
    taskHTML, taskCSS, taskJS, taskScript, devImages, devFonts, livePreview, watchFiles
);