// Global Variables
const textDefault = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.';
const textMd = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.';
const textLg = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.';
const textXl = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.';


(function () {

    // (function (){ console.log('root js load only link file'); }); [0]
    // <script>console.log('blank js load');</script> [1]
    // document.addEventListener('alpine:init', function (){ console.log('alpine init js load'); }); [2]
    // $(function() { console.log('jquery blank js load'); }); [3]
    // $(document).ready(function() { console.log('jquery document js load'); }); [3]
    // window.addEventListener('load', function (){ console.log('alpine window js load'); }); [4]
    // $(window).on("load", function() { console.log('jquery window js load'); }); [5]


    // Raw JS
    window.addEventListener('load', function () {
        // screen loader
        const screen_loader = document.getElementsByClassName('screen_loader');
        if (screen_loader?.length) {
            screen_loader[0].classList.add('transition');
            setTimeout(() => {
                document.body.removeChild(screen_loader[0]);
            }, 200);
        }
    });


    // Alpine JS [run first raw js]
    document.addEventListener('alpine:init', async () => {
        console.log("--- Hello Alpine Init ---");

        // dark mode
        Alpine.store('darkMode', {
            on: Alpine.$persist(false).as('darkMode'),
            init() {
                // this.on = window.matchMedia('(prefers-color-scheme: dark)').matches
                // this.on = JSON.parse(localStorage.getItem('darkMode'));
                // if (this.on) {
                //     document.documentElement.classList.add('dark');
                // } else {
                //     document.documentElement.classList.remove('dark');
                // }
                // console.log('init this.on: '+ this.on);
            },
            toggle() {
                this.on = !this.on;
                // if (this.on) {
                //     document.documentElement.classList.add('dark');
                // } else {
                //     document.documentElement.classList.remove('dark');
                // }
            },

        });

        // Load items async global
        Alpine.store('itemStore',{
            isLoading: true,
            search: '',
            category_name: null,
            promote_status: false,
            favorite: false,
            favorites: Alpine.$persist([]).as('favorites_id'),
            carts: Alpine.$persist([]).as('carts'),
            cashInUSD: 0,
            cashInKHM: 0,
            cashOutUSD: 0,
            cashOutKHM: 0,
            receiptNo: null,
            receiptDate: null,
            items: [],
            // init() {
            //     this.updateChange();
            // },
            async load(url) {
                await fetch(url, {
                    method: 'GET',
                })
                    .then((response) => response.json())
                    .then((items) => {
                            this.items = items;
                            this.isLoading = false;
                        }
                    );
            },
            get startItems() {
                return this.items.filter(i => i.name.toLowerCase().startsWith(this.search.toLowerCase()));
            },
            get containItems() {
                return this.items.filter(i => i.name.toLowerCase().includes(this.search.toLowerCase()));
            },

            get equalCategoryName() {
                return this.items.filter(i => i.category_name.toLowerCase() === this.category_name.toLowerCase());
            },
            get equalPromote() {
                return this.items.filter(i => i.promote_status === this.promote_status);
            },

            checkFavorite(id){
                let check = this.favorites.find(i => i === id);
                return check === id;
            },

            get equalFavorite() {
                let myStr = "";
                let count_favorites = this.favorites.length;
                let count_for_each = 0;
                this.favorites.forEach(function (item, index){
                    count_for_each++;
                    if (count_favorites !== count_for_each){
                        myStr += "(i['id'] === "+item+") || ";
                    } else {
                        myStr += "(i['id'] === "+item+")";
                    }
                });

                return this.items.filter(function (i){
                    return eval(myStr);
                });

                // return this.items.filter(i => (i['id'] === 2) || (i['id'] === 3));

            },

            addFavorite(id) {
                const index = this.favorites.findIndex((i) => i === id);
                if (index === -1) {
                    this.favorites.unshift(id);
                }
            },
            removeFavorite(id) {
                const index = this.favorites.indexOf(id);
                if (index !== -1) {
                    this.favorites.splice(index, 1);
                }


                // this.favorites = this.favorites.sort();
                // console.log(this.favorites);
                //
                // let i = this.favorites.length;
                // while (--i) {
                //     if (this.favorites[i] === this.favorites[i-1]) {
                //         this.favorites.splice(i, 1);
                //     }
                //     let index = this.favorites.indexOf(id);
                //     if (index !== -1) {
                //         this.favorites.splice(index, 1);
                //     }
                // }
            },

            get filterItems(){
                if(this.search !== null){
                    return this.containItems;
                } else if(this.promote_status) {
                    return this.equalPromote;
                } else if(this.favorite) {
                    return this.equalFavorite;
                } else if(this.category_name !== null) {
                    return this.equalCategoryName;
                }
            },

            addToCart(item) {
                const index = this.carts.findIndex((i) => i.id === item.id);
                if (index === -1) {
                    this.carts.unshift({
                        id: item.id,
                        image: item.logo.logo_thumbnail,
                        name: item.name,
                        weight: item.weight,
                        code: item.code,
                        price: item.price,
                        discount: 0,
                        qty: 1,
                    });
                } else {
                    this.carts[index].qty += 1;
                }
                this.beep();
                this.updateChange();
            },
            getItemsCount() {
                return this.carts.reduce((count, item) => count + item.qty, 0);
            },
            addQty(item, qty) {
                const index = this.carts.findIndex((i) => i.id === item.id);
                if (index === -1) {
                    return;
                }
                const afterAdd = parseFloat(item.qty) + qty;
                if (afterAdd === 0) {
                    this.carts.splice(index, 1);
                    this.clearSound();
                } else {
                    this.carts[index].qty = afterAdd;
                    this.beep();
                }
                this.updateChange();
            },

            activeCart: {},
            editActiveCart(item) {
                // this.activeCart = this.carts.find((i) => {return i.id === item.id});
                this.activeCart = item;
            },

            getTotalPrice() {
                return this.carts.reduce((total, item) => total + item.qty * item.price, 0);
            },
            addCashUSD(amount) {
                this.cashInUSD = (this.cashInUSD || 0) + amount;
                this.updateChange();
                this.beep();
            },
            updateChange() {
                this.cashOutUSD = (this.cashInUSD || 0) - this.getTotalPrice();
            },
            updateCash(value = 0) {
                // this.cashInUSD = parseFloat(value.replace(/[^0-9]+/g, ""));
                this.cashInUSD = parseFloat(value.replaceAll(',', ''));
                this.updateChange();
            },
            get isSubmit() {
                return this.cashOutUSD >= 0 && this.carts.length > 0;
            },
            submit() {
                const time = new Date();
                this.receiptNo = `INV-${Math.round(time.getTime() / 1000)}`;
                this.receiptDate = this.dateFormat(time);
            },

            dateFormat(date) {
                const formatter = new Intl.DateTimeFormat('id', { dateStyle: 'short', timeStyle: 'short'});
                return formatter.format(date);
            },
            clear() {
                this.cashInUSD = 0;
                this.carts = [];
                this.receiptNo = null;
                this.receiptDate = null;
                this.updateChange();
                this.clearSound();
            },
            beep() {
                this.playSound("/assets/sound/beep-29.mp3");
            },
            clearSound() {
                this.playSound("/assets/sound/button-21.mp3");
            },
            playSound(src) {
                const sound = new Audio();
                sound.src = src;
                sound.play();
                sound.onended = () => delete(sound);
            },
            printInvoice() {
                const receiptContent = document.getElementById('receipt-content');
                const titleBefore = document.title;
                const printArea = document.getElementById('print-area');

                printArea.innerHTML = receiptContent.innerHTML;
                document.title = this.receiptNo;

                window.print();

                printArea.innerHTML = '';
                document.title = titleBefore;

                // TODO save sale data to database

                this.clear();
            }

        });

        // Load items async different
        Alpine.data('itemData', (initialUrl = '') => ({
            selected: -2,
            isLoading: true,
            search: '',
            items: [],
            init() {
                this.load(initialUrl);
            },

            async load(url) {
                await fetch(url, {
                    method: 'GET',
                })
                    .then((response) => response.json())
                    .then((items) => {
                            this.items = items;
                            this.isLoading = false;
                        }
                    );
            },

            get containItems() {
                return this.items.filter(i => i.name.toLowerCase().includes(this.search.toLowerCase()));
            },


        }));

        Alpine.data('dropMe', (initialOpenState = 1) => ({
            num: initialOpenState,
            open: false,
            init() {
                console.log('init first load: ' + this.num);
                // this.loadFirst();

                this.$watch('num', () => { //like x-effect at frontend, x-effect will load first time after finished initial script for watch all properties used within it
                    console.log(this.num);
                })
            },

            loadFirst(numb){
                this.num = numb;
            },

            trigger: {
                ['@click']() {
                    this.num = 1;
                    this.open = !this.open;
                },
            },
            dialogue: {
                ['x-show']() {
                    return this.open
                },
            },
        }));

        // drop down
        Alpine.data('dropdown', () => ({
            open: false,

            toggle() {
                this.open = ! this.open
            },
        }));

        // scroll to top
        Alpine.data('scrollToTop', () => ({
            showTopButton: false,
            init() {
                window.onscroll = () => {
                    this.scrollFunction();
                    // console.log("Kimlay scrolled");
                };
            },

            scrollFunction() {
                if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                    this.showTopButton = true;
                } else {
                    this.showTopButton = false;
                }
            },

            goToTop() {
                window.scrollTo({ top: 0, behavior: 'smooth' });
                // document.body.scrollTop = 0;
                // document.documentElement.scrollTop = 0;
            },
        }));



    });





})();
