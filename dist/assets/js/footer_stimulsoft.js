Stimulsoft.Base.StiLicense.key = "6vJhGtLLLz2GNviWmUTrhSqnOItdDwjBylQzQcAOiHn0s4gy0Fr5YoUZ9V00Y0igCSFQzwEqYBh/N77k4f0fWXTHW5rqeBNLkaurJDenJ9o97TyqHs9HfvINK18Uwzsc/bG01Rq+x3H3Rf+g7AY92gvWmp7VA2Uxa30Q97f61siWz2dE5kdBVcCnSFzC6awE74JzDcJMj8OuxplqB1CYcpoPcOjKy1PiATlC3UsBaLEXsok1xxtRMQ283r282tkh8XQitsxtTczAJBxijuJNfziYhci2jResWXK51ygOOEbVAxmpflujkJ8oEVHkOA/CjX6bGx05pNZ6oSIu9H8deF94MyqIwcdeirCe60GbIQByQtLimfxbIZnO35X3fs/94av0ODfELqrQEpLrpU6FNeHttvlMc5UVrT4K+8lPbqR8Hq0PFWmFrbVIYSi7tAVFMMe2D1C59NWyLu3AkrD3No7YhLVh7LV0Tttr/8FrcZ8xirBPcMZCIGrRIesrHxOsZH2V8t/t0GXCnLLAWX+TNvdNXkB8cF2y9ZXf1enI064yE5dwMs2fQ0yOUG/xornE";
var designer = null;

var report = new Stimulsoft.Report.StiReport();

function lordReportData(data){
    var dataSet = new window.Stimulsoft.System.Data.DataSet("data");
    var options = new Stimulsoft.Viewer.StiViewerOptions();
    options.appearance.theme = Stimulsoft.Viewer.StiViewerTheme.Office2013WhiteBlue;

    options.appearance.scrollbarsMode = true;
    options.toolbar.visible = false;
    options.height = "500px";

    options.toolbar.printDestination = Stimulsoft.Viewer.StiPrintDestination.Direct;

    var viewer = new Stimulsoft.Viewer.StiViewer(options, "StiViewer", false);
    report.loadFile("/pages/report/invoice_receipt_default.mrt");

    viewer.showProcessIndicator();
    dataSet.readJson(data);//one JSON object

    //this line of code added,too.
    report.dictionary.databases.clear();



    report.regData("data", "data", dataSet);
    report.dictionary.synchronize();

    viewer.report = report;
    viewer.renderHtml("content");
}



function myPrint(){
    report.renderAsync(function(){report.print();});
}