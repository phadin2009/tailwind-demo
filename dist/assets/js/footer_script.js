function kimSplitNumber(number, digit = 2) {
    if (typeof number === "number") {
        number = number.toString();
    }
    number = number.split('.');

    if (number.length > 1) {
        return [
            number[0].replace(/\B(?=(\d{3})+(?!\d))/g, ","),
            parseFloat('0.' + number[1]).toFixed(digit).split('.')[1],
        ];
    } else {
        return [
            number[0].replace(/\B(?=(\d{3})+(?!\d))/g, ","),
            '00'
        ];
    }
}

function kimFormatNumber(number, digit = 2) {
    if(number == null) {
        number = 0;
    }
    return number.toLocaleString('en-US', {minimumFractionDigits: digit, maximumFractionDigits: digit});
}

function kimCash() {
    return {
        cashKHR: false,
        storeUSD: [1, 5, 10, 20, 50, 100],
        storeKHR: [100, 500, 1000, 5000, 10000, 50000],
        toggle() {
            this.cashKHR = !this.cashKHR;
        },
    }
}
